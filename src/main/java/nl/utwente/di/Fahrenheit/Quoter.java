package nl.utwente.di.Fahrenheit;

public class Quoter {
    /** Converts the Celsius to Fahrenheit*/
    public double getFahrenheit(Double celsius) {
        return (celsius *1.8) +32;
    }
}
